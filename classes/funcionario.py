from pessoa import Pessoa
from salario import Salario


class Funcionario(Pessoa):

    def __init__(self):
        self.matricula = 0
        self.setor = ""
        self.cargo = ""
        self.nivel = ""
        self.funcionario = {}
        self.salario = 0
        super().__init__()

    def cadastrarFuncionario(self, data=None, nivelprof=None, hierarquia="Professor"):
        func = {}
        func = self.funcionario
        self.matricula = int(input("informe a matrícula do funcionário: "))
        if nivelprof is None:
            self.nivel = str(input("Informe o nível do funcionário: "))
        else:
            self.nivel = nivelprof

        self.setor = str(input("Informe o setor do funcionário: "))
        self.cargo = str(input("Informe o cargo do funcionário: "))

        print("\nAs perguntas a seguir serão relacionadas ao salario do funcionário, caso falte alguma das informações, preencha com o valor 0 (leve em consideração que isso irá interferir no calculo final do salário, exceto no que se refere ao valor do salário Bruto)")
        inss = float(input("Valor do INSS: "))
        irrf = float(input("Valor do IRRF: "))

        Salary = Salario()
        if self.nivel.upper() == "A":
            self.salario = Salary.CalcularSalario(1520.25, inss, irrf)
        elif self.nivel.upper() == "B":
            self.salario = Salary.CalcularSalario(2362.67, inss, irrf)
        elif self.nivel.upper() == "C":
            self.salario = Salary.CalcularSalario(2988.92, inss, irrf)
        elif self.nivel.upper() == "D":
            self.salario = Salary.CalcularSalario(3572.77, inss, irrf)
        elif self.nivel.upper() == "E":
            self.salario = Salary.CalcularSalario(4878.67, inss, irrf)
        elif self.nivel.upper() == "I":
            self.salario = Salary.CalcularSalario(6500.00, inss, irrf)
        elif self.nivel.upper() == "II":
            self.salario = Salary.CalcularSalario(8325.50, inss, irrf)
        elif self.nivel.upper() == "III":
            self.salario = Salary.CalcularSalario(12568.42, inss, irrf)
        else:
            print("O nível deve ser: A, B, C, D ou E, ou se for professor: I, II, III, verifique o nível entrado e tente novamente!")

        if self.nivel.upper() != "I" or self.nivel.upper() != "II" or self.nivel.upper() != "III":
            func.update({str(self.nome): str(self.nome), "Matricula": self.matricula, "Setor": self.setor,
                                     "Cargo": self.cargo, "Nivel": self.nivel, "Salario": self.salario[0]})
            self.Cadastrar(self.funcionario)
        else:
            func.update({str(self.nome): str(self.nome), "Matricula": self.matricula, 
            "Nivel": self.nivel, "Salary": self.salario, "Cargo": f"{hierarquia}: {data}"})

    def exibirFuncionario(self):
        super().Exibir(self.funcionario)
