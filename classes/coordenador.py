from professor import Professor

global prof


class Coordenador:
    def __init__(self):
        self.area = ""
        self.plusSalario = 0.15
        self.salario = 0
        self.prof = Professor()   

    def cadastrarCoordenadorProf(self, coord="Coordenador_Professor"):
        prof = self.prof
        self.area = str(input("Qual a área do Coordenador: "))
        self.salario = float(input("Informe o salário do Coordenador: "))
        prof.cadastrarProfessor(self.area, "Coordenador_Professor")      

    def exibirCoordenadorProf(self):
        prof = self.prof
        op = str(input("Para informações administrativas sobre o coordenador, digite: a, ou, digite: c para informações curriculares sobre o coordenador:   "))
        if op.lower() == "c":
            print(f"\n Area: {self.area}|\n\t¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
            print(F"Salario: {self.calcularPlusSalario(self.plusSalario)}")
        else:
            prof.exibirProfessor(op)
            print(f"|\tArea: {self.area}\n|\t|Plus Salario: {self.calcularPlusSalario(self.plusSalario)}\n|\t¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")

    def calcularPlusSalario(self, var):
        prof = self.prof
        self.salario += (self.salario * var)
        return self.salario
