class Salario:
    def __init__(self):
        self.salarioBruto = 0
        self.inss = 0.0
        self.irrf = 0.0
        self.salarioLiquido = 0.0
        self.planoSaude = 0.0

    def CalcularSalario(self, salario, valorInss, valorIrrf):
        self.salarioBruto = salario
        self.inss = self.salarioBruto * valorInss
        self.irrf = valorIrrf * (self.inss - self.salarioBruto)
        self.salarioLiquido = self.salarioBruto - (self.inss + self.irrf)
        self.planoSaude = self.salarioLiquido - 125

        return (self.salarioBruto, self.salarioLiquido, self.inss, self.irrf, self.planoSaude)
