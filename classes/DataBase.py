import pickle
from pathlib import Path


class DataBase:
    def __init__(self, out_file):
        self.out_file = out_file

    def Save(self, filename):
        if Path(self.out_file).exists():
            self.Update(filename)
        else:
            pickle.dump(filename, open(self.out_file, "ab"), pickle.HIGHEST_PROTOCOL)

    def Update(self, filename):
        pickle.dump(filename, open(self.out_file, "wb"), pickle.HIGHEST_PROTOCOL)   
 
    def Load(self):
        loaded_files = pickle.load(open(self.out_file, "rb"))
        return loaded_files

    def PrintFile(self):
        loader = self.Load()
        for i in loader:
            for k, v in loader.items():
                print(f"---------------------\n| {k}\n|")
                for a, b in v.items():
                    print(f"|\t{a}:{b}")
                print("|\n---------------------")
