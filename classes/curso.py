from professor import Professor
from pessoa import Pessoa
class Curso:
    def __init__(self):
        self.titulo = ""
        self.descricao = ""
        self.valor = 865.23
        self.sala = ""
        self.prof = Professor()
        self.curso = {}
        self.pes = Pessoa()
    
    def cadastrarCurso(self):
        self.titulo = str(input("Informe titulo do curso: "))
        self.descricao = str(input("Informe a descrição do curso: "))
        self.valor = float(input("Informe valor do curso: "))
        self.sala = str(input("Informe a sala do curso: "))
        
        self.curso.update({"Titulo": self.titulo, "Descricao": self.descricao, "Valor": self.valor, "Sala": self.sala})
        op = str(input("Gostaria de cadastrar um professor nessa disciplina? Digite S para Sim, ou N para Não: "))
        if op.upper() == "S":
            self.prof.cadastrarProfessor(self.curso, "Professor")
            
    def exibirCurso(self):
        print()
        self.pes.interate(self.curso)

    def calcNumMinAluno(self, val=865.23, prof=12568.43):
        minAl = int(val/prof) + 1
        return minAl
