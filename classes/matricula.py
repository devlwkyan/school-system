from aluno import Aluno


class Matricula:
    def __init__(self):
        self.id = 0
        self.mesMatricula = 0
        self.anoMatricula = 0
        self.matricula = {}

    def matricular(self):
        self.id = int(input("Informe ID do aluno: "))
        self.mesMatricula = int(input("Informe mês (número) da matrícula do aluno: "))
        self.anoMatricula = int(input("Informe o ano de matrícula do aluno: "))
        self.matricula.update({"ID": self.id, "Mes_Matricula": self.mesMatricula, "Ano_Matricula": self.anoMatricula})
        
        Alunos = Aluno()
        Alunos.cadastrarAluno(self.matricula)

