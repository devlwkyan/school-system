from pessoa import Pessoa
from aluno import Aluno
from coordenador import Coordenador
from coordenadoradm import CoordenadorAdm
from matricula import Matricula
from professor import Professor
from time import sleep

class Main:
    def __init__(self):
        self.pes = Pessoa()
        self.al = Aluno()
        self.cor = Coordenador()
        self.cord = CoordenadorAdm()
        self.mat = Matricula()
        self.prof = Professor()


    def menu(self):
        print("""
        
        O que deseja fazer: 
            [0] - Cadastrar Professores
            [1] - Cadastrar Coordenadores
            [2] - Cadastrar Aluno
            [3] - Matricular Aluno
            [4] - Listar Professor
            [5] - Listar Coordenador
            [6] - Listar Alun
            [7] - Curso e detalhes
            [8] - Demo do Aplicativo
        """)

        op = int(input("Digite o número referente a opção: "))
        if op == 0:
            self.prof.Cadastrar()
            self.menu()
        elif op == 1:
            x = input("Coordenador Administrativo = A, ou Professor = P").upper()
            if x == "A":
                self.cord.cadastrarCoordenadorAdm()
                self.menu()
            else:
                self.cor.cadastrarCoordenadorProf()
                self.menu()
        elif op == 2:
            self.al.cadastrarAluno()
            self.menu()
        elif op == 3:
            self.mat.matricular()
            self.menu()
        elif op == 4:
            self.prof.exibirProfessor()
            self.menu()
        elif op == 5:
            print("Coordenador Professor: \n")
            self.cor.exibirCoordenadorProf()
            print("\nCoordenador Administrador: \n")
            self.cord.exibirCoordenadorAdm()
            self.menu()
        elif op == 6:
            self.al.exibirAluno()
            self.menu()
        elif op == 7:
            pass
        elif op == 8:
            self.pes.Cadastrar()
            x = input("Agora digite exibir para vermos o que foi salvo: ")
            self.pes.Exibir()
            print("Parabens, terminamos o demo do aplicativo")
            sleep(1)
            self.menu()

Menu = Main()
Menu.menu()
