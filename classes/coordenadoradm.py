from coordenador import Coordenador


class CoordenadorAdm(Coordenador):
    def __init__(self):
        self.plusSalario = 0.0
        self.area = ""
        super().__init__()

    def cadastrarCoordenadorAdm(self):
        super().cadastrarCoordenadorProf("Coordenador_Administrador")
    
    def exibirCoordenadorAdm(self):
        super().exibirCoordenadorProf(self.calcularPlusSalario())

    def calcularPlusSalario(self):
        self.plusSalario = float(input("Informe o valor do acrescimo ao salário do Coordenadoer Administrador: "))
        super().calcularPlusSalario(self.plusSalario)

