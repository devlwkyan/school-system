class Pessoa:
    def __init__(self):
        self.nome = ""
        self.rg = ""
        self.cpf = ""
        self.anoNasc = 0
        self.mesNasc = 0
        self.diaNasc = 0
        self.genero = ""
        self.pessoas = {}
  
    def Cadastrar(self, dadospessoa=None):
        self.nome = str(input("Informe o nome: "))
        self.rg = str(input("Informe o RG: "))
        self.cpf = str(input("Informe o CPF: "))
        self.diaNasc = int(input("Informe o dia do nascimento: "))
        self.mesNasc = int(input("Informe o mês (número) de nascimento: "))
        self.anoNasc = int(input("Informe o ano de nascimento: "))
        self.genero = str(input("Informe o gênero: "))
        self.pessoas.update({self.nome: {"RG": self.rg, "CPF": self.cpf, "Dia_Nascimento": self.diaNasc, "Mes_Nascimento": self.mesNasc, "Ano_Nascimento": self.anoNasc, "Genero": self.genero, "Outros_Dados": dadospessoa}})
     
    def Exibir(self, data=None):
        dictionary = data
        if dictionary is not None:
            print(f"\n________________________________\n|\n| {list(dictionary)[0]}\n|-------------------------------\n|")
            self.interate(dictionary)
            self.interate(self.pessoas)
            print("|_______________________________")
        else:
            dictionary = self.pessoas
            print(f"________________________________\n|\n| {list(dictionary)[0]}\n|---------    ----------------------\n|")
            self.interate(dictionary)
            print("|_______________________________")
    
    def interate(self, d):
        for k, v in d.items():    
            if isinstance(v, dict):
                self.interate(v)
            else:
                print(f"|\t|{k}: {v}\n|\t¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")

