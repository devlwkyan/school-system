from funcionario import Funcionario
from DataBase import DataBase


class Professor(Funcionario):
    def __int__(self):
        self.formacao = ""
        self.disciplina = ""
        self.nivel = ""
        self.professord = {}
        super().__init__()

    def cadastrarProfessor(self, data=None, coord="Coordenador"):
        db = DataBase("Professor.pickle")
        prof = {}
        self.professord = prof
        self.formacao = str(input("Informe a formação do professor: "))
        self.disciplina = str(input("Informe a disciplina do professor: "))
        self.nivel = str(input("Informe o nível do professor: "))
        if data is not None:
            prof.update({"Formacao": self.formacao, "Disciplina": self.disciplina, "Nivel": self.nivel, "Grau_Hierarquico": f"{coord}: (Area= {data})"})
            db.Save(prof)
            self.cadastrarFuncionario(prof, self.nivel)
        else:
            self.cadastrarFuncionario(None, self.nivel)     
       
    def exibirProfessor(self, op=None):
        db = DataBase("Professor.pickle")
        prof = self.professord

        if op is None:
            op = str(input("Para informações administrativas, digite: a, ou, digite: c para informações curriculares: "))
        
        if op.lower() == "c":
            db.PrintFile()
        else:
            self.exibirFuncionario()
            for k, v in prof.items():
                print(f"|\t|{k}: {v}\n|\t¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
