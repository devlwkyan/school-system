import sys
sys.path.append('classes')
from classes.welcome import Welcome
from classes.pessoa import Pessoa
from classes.aluno import Aluno
from classes.coordenador import Coordenador
from classes.coordenadoradm import CoordenadorAdm
from classes.matricula import Matricula
from classes.professor import Professor
from classes.curso import Curso
from time import sleep

class Main:
    def __init__(self):
        self.pes = Pessoa()
        self.al = Aluno()
        self.cor = Coordenador()
        self.cord = CoordenadorAdm()
        self.mat = Matricula()
        self.prof = Professor()
        self.wel = Welcome()
        self.curso = Curso()

    def menu(self):
        self.wel
        print("""
        
        O que deseja fazer: 
            [1] - Cadastrar Professores
            [2] - Cadastrar Coordenadores
            [3] - Cadastrar Aluno
            [4] - Matricular Aluno
            [5] - Listar Professor
            [6] - Listar Coordenador
            [7] - Listar Aluno
            [8] - Curso e detalhes
            [9] - Demo do Aplicativo
            [0] - Sair do Aplicativo
            [?] - Ajuda
            """)

        op = input("Digite o número referente a opção: ")
        if op == "1":
            self.prof.Cadastrar()
            self.menu()
        elif op == "2":
            x = input("Coordenador Administrativo = A, ou Professor = P").upper()
            if x == "A":
                self.cord.cadastrarCoordenadorAdm()
                self.menu()
            else:
                self.cor.cadastrarCoordenadorProf()
                self.menu()
        elif op == "3":
            self.al.cadastrarAluno()
            self.menu()
        elif op == "4":
            self.mat.matricular()
            self.menu()
        elif op == "5":
            self.prof.exibirProfessor()
            self.menu()
        elif op == "6":
            print("Coordenador Professor: \n")
            self.cor.exibirCoordenadorProf()
            print("\nCoordenador Administrador: \n")
            self.cord.exibirCoordenadorAdm()
            self.menu()
        elif op == "7":
            self.al.exibirAluno()
            self.menu()
        elif op == "8":
            op = str(input("Gostaria de cadastrar (digite C), listar os cursos (digite L), ou quantidade minima de alunos para o curso (digite Q): "))
            if op.lower() == "c":
                self.curso.cadastrarCurso()
                self.menu()
            elif op.lower() == "l":
                self.curso.exibirCurso()
                self.menu()
            elif op.lower() == "q":
                x = float(input("Informe o salário do professor: "))
                self.curso.calcNumMinAluno(x)
                self.menu()
            else:
                print("Opção inválida, voltando ao menu principal")
                sleep(1)
                self.menu()
        elif op == "9":
            self.pes.Cadastrar()
            x = input("Agora digite exibir para vermos o que foi salvo: ")
            self.pes.Exibir()
            print("Parabens, terminamos o demo do aplicativo")
            sleep(2)
            self.menu()
        elif op == "0":
            print("Tchau tchau!")
            pass
        elif str(op) == "?":
            self.help()
            self.menu()

Menu = Main()
Menu.menu()
